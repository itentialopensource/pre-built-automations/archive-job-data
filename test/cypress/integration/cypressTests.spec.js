import { WorkflowRunner, PrebuiltRunner } from '@itential-tools/iap-cypress-testing-library/testRunner/testRunners';
const ArchiveJobsandTasksCollectionsJob0Data = require('../fixtures/stubs/Archive Jobs and Tasks Collections Job0.json');

function initializeWorkflowRunner(workflow, importWorkflow, isStub, stubTasks) {
  let workflowRunner = new WorkflowRunner(workflow.name);

  if (importWorkflow) {
    // cancel all running jobs for workflow
    workflowRunner.job.cancelAllJobs();

    workflowRunner.deleteWorkflow.allCopies({
      failOnStatusCode: false
    });
    // Check if Stub flag is enabled
    if(isStub){
      stubTasks.forEach(stubTask=>{
        workflow = workflowRunner.stub.task({
          stub: stubTask,
          workflow: workflow,
        });
      })
    }
    workflowRunner.importWorkflow.single({
      workflow,
      failOnStatusCode: false
    });
  }

  /* Verify workflow */
  workflowRunner.verifyWorkflow.exists();
  workflowRunner.verifyWorkflow.hasNoDuplicates({});
  // workflowRunner.verifyWorkflow.dependenciesOnline();

  return workflowRunner;
}
// Function to delete the stubbed workflow and reimport it without the stub tasks
function replaceStubTasks(workflowRunner, workflowName) {
  workflowRunner.deleteWorkflow.allCopies({
      failOnStatusCode: false,
  });
  workflowRunner.importWorkflow.single({ workflow: workflowName });
  workflowRunner.verifyWorkflow.exists();
  workflowRunner.verifyWorkflow.hasNoDuplicates({});
}

describe('Default: Cypress Tests', function () {
  let prebuiltRunner;
  let ArchiveJobsandTasksCollectionsJob0Workflow;

  before(function () {
    //creates a prebuilt runner for importing the prebuilt
    cy.fixture(`../../../artifact.json`).then((data) => {
      prebuiltRunner = new PrebuiltRunner(data);
    });
    cy.fixture(`../../../bundles/workflows/Archive Jobs and Tasks Collections.json`).then((data) => {
      ArchiveJobsandTasksCollectionsJob0Workflow = data;
    });
  });

  after(function() {
    prebuiltRunner.deletePrebuilt.single({ failOnStatusCode: false });
  });

  describe('Default: Imports Pre-Built', function () {
    // eslint-disable-next-line mocha/no-hooks-for-single-case
    it('Default: Should import the prebuilt into IAP', function () {
        prebuiltRunner.deletePrebuilt.single({ failOnStatusCode: false });
        prebuiltRunner.importPrebuilt.single({});
    });
  });

  describe('Archive Jobs and Tasks Collections', function() {
    it('It should archive jobs and tasks', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(ArchiveJobsandTasksCollectionsJob0Workflow, importWorkflow, isStub, ArchiveJobsandTasksCollectionsJob0Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: ArchiveJobsandTasksCollectionsJob0Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(ArchiveJobsandTasksCollectionsJob0Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          delete jobVariables.queryDoc.filter["metrics.start_time"];
          expect(jobVariables).eql(ArchiveJobsandTasksCollectionsJob0Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, ArchiveJobsandTasksCollectionsJob0Workflow);
      });
    })
  });

});
